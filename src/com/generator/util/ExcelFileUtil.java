/**
 * 
 */
package com.generator.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * @author prateek.tawal
 *
 */
public class ExcelFileUtil {

	static org.apache.log4j.Logger log = 
			org.apache.log4j.Logger.getLogger(Utilities.class.getName());

	/**
	 * Method to READ excel file and create a data map
	 * @param path
	 * @return
	 */
	public List<Map<String, String>> readFile(String path) {

		List<Map<String, String>> dataList = null;
		try{
			FileInputStream file = new FileInputStream(path);
			if(file != null){
				dataList = new ArrayList<Map<String,String>>();
				HSSFWorkbook workbook = new HSSFWorkbook(file);
				//TODO: Handle multiple sheets if required
				HSSFSheet sheet = workbook.getSheetAt(5);
				Iterator<Row> rowIterator = sheet.iterator();
				//Iterate each row from a sheet
				while(rowIterator.hasNext()){
					Row row = rowIterator.next();
					Iterator<Cell> cellIterator = row.cellIterator();
					//Iterate each cell from a row
					while(cellIterator.hasNext()){
						Cell cell = cellIterator.next();
						//cell.getCellType();
						System.out.println("-> "+cell.getStringCellValue());
					}	
				}
			}
		}catch(IOException ex){
			log.error("Excel file READ ERROR at location "+path);
			ex.printStackTrace();
		}
		return dataList;
	}

}

