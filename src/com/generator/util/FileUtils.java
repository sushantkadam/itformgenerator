
/**
 * 
 */
package com.generator.util;

import java.io.File;

/**
 * @author prateek.tawal
 * This class performs basic File operations like Read and Write
 */
public class FileUtils {
	
	static org.apache.log4j.Logger log = 
			org.apache.log4j.Logger.getLogger(FileUtils.class.getName());
	
	/**
	 * @param path
	 * @return
	 */
	public File readFile(String path){
		
		File file = new File(path);
		if(file.isFile()){
			return file;
		}else{
			log.error("File read ERROR at location "+path);
		}
		return null;
	}
	
	/**
	 * @param file
	 * @return
	 */
	public Boolean writeFile(File file){
		//TODO:
		return null;
	}

}
