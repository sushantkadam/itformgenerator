/**
 * 
 */
package com.generator.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author prateek.tawal
 *
 */
public class Utilities {

	static org.apache.log4j.Logger log = 
			org.apache.log4j.Logger.getLogger(Utilities.class.getName());
	static Properties prop;
	static{
		prop = new Properties();
		try {
			prop.load(new FileInputStream(SystemConstants.configFilePath));
		} catch (IOException ex) {
			log.error("config.properties FILE NOT FOUND at location C:/");
			ex.printStackTrace();
		} 
	}

	/**
	 * Method to return value of given property key
	 * @param key
	 * @return
	 */
	public static String getPropertyValue(String key){
		String value = null;
		value = prop.getProperty(key);
		if(value != null){
			return value;
		}else{
			log.error("Property "+key+" NOT FOUND or INCORRECT in config.properties");
		}
		return null;
	}

}
