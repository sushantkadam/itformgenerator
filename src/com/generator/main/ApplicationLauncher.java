package com.generator.main;

import java.util.List;
import java.util.Map;

import com.generator.util.ExcelFileUtil;
import com.generator.util.SystemConstants;
import com.generator.util.Utilities;

/**
 * @author prateek.tawal
 *
 */
public class ApplicationLauncher {

	public static void main(String[] args) {
		
		List<Map<String, String>> dataList = excelFileRead();
		
		pdfFileWrite(dataList);
	}
	
	public static List<Map<String, String>> excelFileRead(){
		
		ExcelFileUtil excelFileUtil = new ExcelFileUtil();
		return excelFileUtil.readFile(
				Utilities.getPropertyValue(SystemConstants.excelFilePath));
	}
	
	public static Boolean pdfFileWrite(List<Map<String, String>> dataList){
		
		//TODO: PDF file write
		return null;
	}

}
